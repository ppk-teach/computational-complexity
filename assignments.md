# Assignment 1

1. For a language $A$ recall the definition of $∃A$. Prove that
   the class $∃ L = NP$. Hint: Show that 3-SAT is in $∃ L$.

   This shows that the read-once restriction is crucial in the
   definition of NL in terms of the complexity class L.

2. Prove that 2-SAT is in NL. Hint: consider any 2-SAT clause $x₁ ∨
   x₂$ as the $¬x₁ → x₂$ which inturn as a directed edge.
