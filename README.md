# Computational Complexity

This is the git repository of my computational complexity course.

## Getting a copy.

The notes here are maintained under the version controller git and
hosted on
[bitbucket](https://bitbucket.org/ppk-teach/computational-complexity/). You
can clone it using the following command.

```bash
git clone https://bitbucket.org/ppk-teach/computational-complexity/

```

## Licenses and Copyright

(C) Piyush P Kurur, 2016


All notes, reference material, in particular any non-executable files
is released under *Creative Commons Attribution-ShareAlike*
license. Any software/program, scripts or any form of executable that
is distributed with this repository is under the terms of the 2-clause
BSD license.

For the exact terms and conditions of these licences, see the
respective files in the sub-directory `licences`.

I welcome contributions from you. You need to do so by sending me an
appropriate pull request. However, if you choose to contribute, it is
understood that

1. You agree to the release of the material contributed by you under
   the terms and conditions of the above license.

2. You agree to transfer the copyright to me, i.e. Piyush P Kurur. In
   return, I pledge as the primary author of this material, to make
   this and future versions under the same licensing conditions.
